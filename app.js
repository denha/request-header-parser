var http = require('http');

http.createServer(function (request, response) {
    // console.log(request.headers);
    var object = {
        "ip": request.headers['x-forwarded-for'] || request.connection.remoteAddress,
        "language": request.headers['accept-language'].split(',')[0],
        "os": request.headers['user-agent'].match(/\((.+?)\)/)[1]
    };
    response.end(JSON.stringify(object));
}).listen(process.env.PORT || 5000);

